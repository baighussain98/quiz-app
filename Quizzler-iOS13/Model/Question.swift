//
//  Question.swift
//  Quizzler-iOS13
//
//  Created by Hussain-SCT on 21/10/2022.
//  Copyright © 2022 The App Brewery. All rights reserved.
//

import Foundation

struct Question {
    let text : String
    let answer : String
    
    init(q:String, a:String) {
        text = q
        answer = a
    }
}
// 1. What is a view controller?
// 2. What are the view controller life cycle? and there phases?
//    MVC is a coding convention
// 3. What is MVC its advantages and disadvantages?
// 4. Classes vs Structure?
// 5. Application Lifecycle and there phases
// 6. Different types of variable declaration

